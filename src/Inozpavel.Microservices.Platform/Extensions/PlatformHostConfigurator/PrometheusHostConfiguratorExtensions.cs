﻿using Inozpavel.Microservices.Platform.Configurators;
using Prometheus.Client.DependencyInjection;

namespace Inozpavel.Microservices.Platform.Extensions.PlatformHostConfigurator;

internal static class PrometheusHostConfiguratorExtensions
{
    public static IPlatformHostConfigurator AddPlatformPrometheus(this IPlatformHostConfigurator platformHostConfigurator)
    {
        platformHostConfigurator.ConfigureServices(services => services.AddMetricFactory());

        return platformHostConfigurator;
    }
}
