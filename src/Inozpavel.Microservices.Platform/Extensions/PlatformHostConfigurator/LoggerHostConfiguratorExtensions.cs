﻿using Inozpavel.Microservices.Platform.Configurators;
using Inozpavel.Microservices.Platform.Factories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Extensions.Hosting;
using Serilog.Extensions.Logging;
using ILogger = Serilog.ILogger;

namespace Inozpavel.Microservices.Platform.Extensions.PlatformHostConfigurator;

internal static class LoggerHostConfiguratorExtensions
{
    /// <summary>
    /// Позволяет настроить Serilog в качестве логгера
    /// <a href="https://github.com/serilog/serilog-aspnetcore">Serilog repository documentation</a>
    /// </summary>
    public static IPlatformHostConfigurator AddPlatformLogger(this IPlatformHostConfigurator platformHostConfigurator)
    {
        platformHostConfigurator.ConfigureServices(services =>
        {
            services.AddLogging(builder => builder.ClearProviders());
            services.AddTransient<SerilogPlatformLoggerFactory>();
            services.AddSingleton(provider => provider.GetRequiredService<SerilogPlatformLoggerFactory>().CreateLogger());
            services.AddSingleton<ILoggerProvider>(provider =>
                new SerilogLoggerProvider(provider.GetRequiredService<ILogger>()));

            services.AddSingleton(provider =>
            {
                var logger = provider.GetRequiredService<SerilogPlatformLoggerFactory>().CreateLogger();
                Log.Logger = logger;
                return logger;
            });

            // Для логгирования запросов при помощи app.UseSerilogRequestLogging()
            services.AddSingleton(provider =>
            {
                var logger = provider.GetRequiredService<ILogger>();
                var implementationInstance = new DiagnosticContext(logger);
                return implementationInstance;
            }).AddSingleton<IDiagnosticContext>(provider => provider.GetRequiredService<DiagnosticContext>());
        });

        return platformHostConfigurator;
    }
}
