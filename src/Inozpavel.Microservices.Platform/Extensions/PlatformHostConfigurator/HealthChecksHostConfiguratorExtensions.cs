﻿using Inozpavel.Microservices.Platform.Configurators;
using Microsoft.Extensions.DependencyInjection;

namespace Inozpavel.Microservices.Platform.Extensions.PlatformHostConfigurator;

internal static class HealthChecksHostConfiguratorExtensions
{
    public static IPlatformHostConfigurator AddHealthChecks(this IPlatformHostConfigurator platformHostConfigurator)
    {
        platformHostConfigurator.ConfigureServices(services => services.AddHealthChecks());

        return platformHostConfigurator;
    }
}
