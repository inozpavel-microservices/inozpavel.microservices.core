﻿using Inozpavel.Microservices.Platform.Configurators;

namespace Inozpavel.Microservices.Platform.Extensions.PlatformHostConfigurator;

internal static class ConfigurationHostConfiguratorExtensions
{
    public static IPlatformHostConfigurator AddPlatformConfiguration(this IPlatformHostConfigurator platformHostConfigurator)
    {
        platformHostConfigurator.ConfigureConfiguration(builder => builder.DisableFilesSourcesReloadOnChange());

        return platformHostConfigurator;
    }
}
