﻿using Inozpavel.Microservices.Platform.Common;
using Inozpavel.Microservices.Platform.Configurators;
using Inozpavel.Microservices.Platform.Extensions.ApplicationBuilder;
using Inozpavel.Microservices.Platform.Extensions.WebHostBuilder;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

// ReSharper disable UnusedMethodReturnValue.Global

namespace Inozpavel.Microservices.Platform.Extensions.PlatformHostConfigurator.Defaults;

internal static class HostConfiguratorExtensions
{
    public static IPlatformHostConfigurator ConfigureDefaults(
        this IPlatformHostConfigurator platformHostConfigurator,
        PlatformMetadata metadata,
        PlatformEnvironment environment)
    {
        IStartupFilter startupFilter = new PlatformStartupFilter(Configure, metadata, environment);

        platformHostConfigurator.ConfigureContext(metadata.InitializeFromGitlabCiYaml);
        platformHostConfigurator
            .AddPlatformSwagger(environment, metadata)
            .AddPlatformLogger()
            .AddPlatformConfiguration()
            .AddPlatformPrometheus()
            .AddPlatformOpenTracing()
            .AddHealthChecks()
            .ConfigureServices(services => services.AddTransient(_ => startupFilter));

        platformHostConfigurator
            .ConfigureKestrel(options => options.ConfigurePlatformKestrel(environment));
        return platformHostConfigurator;
    }

    private static void Configure(IApplicationBuilder app, PlatformMetadata metadata, PlatformEnvironment environment)
    {
        app
            .UseHealthChecks("/live")
            .UsePlatformDashboard(environment)
            .UsePlatformVersion(metadata)
            .UsePlatformDependencies()
            .UsePlatformSwagger(environment, metadata)
            .UsePlatformPrometheus()
            ;
    }
}
