﻿using System.Reflection;
using Inozpavel.Microservices.Platform.Configurators;
using Jaeger;
using Jaeger.Senders;
using Jaeger.Senders.Thrift;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OpenTracing.Util;

namespace Inozpavel.Microservices.Platform.Extensions.PlatformHostConfigurator;

public static class OpenTracingHostConfigurationExtensions
{
    internal static IPlatformHostConfigurator AddPlatformOpenTracing(
        this IPlatformHostConfigurator platformHostConfigurator)
    {
        platformHostConfigurator.ConfigureServices(services => services
            .AddOpenTracing()
            .AddSingleton(serviceProvider =>
            {
                var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

                Configuration.SenderConfiguration.DefaultSenderResolver = new SenderResolver(loggerFactory)
                    .RegisterSenderFactory<ThriftSenderFactory>();
                const string variable = "JAEGER_SERVICE_NAME";

                if (string.IsNullOrWhiteSpace(Environment.GetEnvironmentVariable(variable)))
                {
                    Environment.SetEnvironmentVariable("JAEGER_SERVICE_NAME", Assembly.GetEntryAssembly()!.FullName);
                }

                var configuration = Configuration.FromEnv(loggerFactory);
                var tracer = configuration.GetTracer();
                GlobalTracer.Register(tracer);
                return tracer;
            }));

        return platformHostConfigurator;
    }
}
