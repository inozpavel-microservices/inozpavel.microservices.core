﻿using Inozpavel.Microservices.Platform.Common;
using Inozpavel.Microservices.Platform.Common.Enums;
using Inozpavel.Microservices.Platform.Configurators;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Inozpavel.Microservices.Platform.Extensions.PlatformHostConfigurator;

internal static class SwaggerHostConfiguratorExtensions
{
    public static IPlatformHostConfigurator AddPlatformSwagger(
        this IPlatformHostConfigurator platformHostConfigurator,
        PlatformEnvironment environment,
        PlatformMetadata platformMetadata)
    {
        if (environment.SwaggerMode != SwaggerMode.Enabled)
        {
            return platformHostConfigurator;
        }

        platformHostConfigurator.ConfigureServices(services =>
        {
            services.AddSwaggerGen(options =>
            {
                options.EnableAnnotations();

                options.CustomSchemaIds(x => x.FullName);
                options.SwaggerGeneratorOptions.DescribeAllParametersInCamelCase = true;

                options.SwaggerDoc("swagger",
                    new OpenApiInfo
                    {
                        Title = platformMetadata.ApplicationName,
                        Version = platformMetadata.ApplicationVersion ?? "1.0.0",
                        Description = platformMetadata.ApplicationDescription,
                        Contact = new OpenApiContact
                        {
                            Email = platformMetadata.ApplicationContactEmail,
                            Name = platformMetadata.ApplicationContactName,
                            Url = Uri.IsWellFormedUriString(platformMetadata.ApplicationContactUrl, UriKind.Absolute)
                                ? new Uri(platformMetadata.ApplicationContactUrl, UriKind.Absolute)
                                : null,
                        },
                    });


                if ((environment.SwaggerSecurity & SwaggerSecurity.JwtBearer) != 0)
                {
                    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                    {
                        In = ParameterLocation.Header,
                        Name = "Authorization",
                        Type = SecuritySchemeType.Http,
                        Description =
                            "JWT authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                        Scheme = "Bearer",
                        BearerFormat = "JWT",
                    });
                }

                if ((environment.SwaggerSecurity & SwaggerSecurity.JwtBearer) != 0)
                {
                    options.AddSecurityRequirement(new OpenApiSecurityRequirement
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Id = "Bearer",
                                    Type = ReferenceType.SecurityScheme,
                                },
                            },
                            new List<string>()
                        },
                    });
                }

                IncludeXmlDocumentationFiles(options);
            });
        });

        return platformHostConfigurator;
    }

    private static void IncludeXmlDocumentationFiles(SwaggerGenOptions swaggerGenOptions)
    {
        Directory
            .GetFiles(AppContext.BaseDirectory, "*.xml", SearchOption.TopDirectoryOnly)
            .ToList()
            .ForEach(xmlFile => swaggerGenOptions.IncludeXmlComments(xmlFile));
    }
}
