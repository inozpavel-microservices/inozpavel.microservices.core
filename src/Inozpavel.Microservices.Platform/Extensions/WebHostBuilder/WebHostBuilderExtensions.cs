﻿using System.Net;
using Inozpavel.Microservices.Platform.Common;
using Microsoft.AspNetCore.Server.Kestrel.Core;

namespace Inozpavel.Microservices.Platform.Extensions.WebHostBuilder;

internal static class WebHostBuilderExtensions
{
    public static void ConfigurePlatformKestrel(this KestrelServerOptions options, PlatformEnvironment environment)
    {
        options.KestrelListen(environment.HttpPort, HttpProtocols.Http1);
        options.KestrelListen(environment.DebugPort, HttpProtocols.Http1);
    }

    private static void KestrelListen(
        this KestrelServerOptions kestrelServerOptions,
        int? port,
        HttpProtocols httpProtocols)
    {
        if (!port.HasValue)
        {
            return;
        }

        kestrelServerOptions.Listen(IPAddress.IPv6Any, port.Value, options => options.Protocols = httpProtocols);
    }
}
