﻿using System.Reflection;
using System.Text.RegularExpressions;
using Inozpavel.Microservices.Platform.Attributes;
using Inozpavel.Microservices.Platform.Common;
using Microsoft.Extensions.Hosting;

namespace Inozpavel.Microservices.Platform.Extensions;

internal static class PlatformMetadataExtensions
{
    private const string GitLabCiFilename = ".gitlab-ci.yml";
    private const string NotCommentPattern = @"\s*[^#]\s*";
    private const string ValueGroupPattern = @"(?<value>.*)";

    private static string? GetFileVariable(string fileContent, string? variableName)
    {
        if (variableName == null)
        {
            return null;
        }
        var match = Regex.Match(
            fileContent,
            GeneratePattern(variableName),
            RegexOptions.Multiline);

        var variableValue = match.Groups["value"].Value.Trim(' ', '\n', '\r', '\'');

        return variableValue;
    }

    private static string GeneratePattern(string variableName) =>
        @$"^{NotCommentPattern}{variableName}\s*:\s*{ValueGroupPattern}$";

    public static void InitializeFromGitlabCiYaml(this PlatformMetadata platformMetadata, HostBuilderContext context)
    {
        var contentPath = context.HostingEnvironment.ContentRootPath;
        InitializeFromGitlabCiYaml(platformMetadata, contentPath, GitLabCiFilename);
    }

    private static void InitializeFromGitlabCiYaml(
        this PlatformMetadata platformMetadata,
        string contentPath, string filename)
    {
        var basePath = contentPath;
        var possiblePath = Path.Combine(basePath, filename);

        while (!File.Exists(possiblePath) && !string.IsNullOrWhiteSpace(basePath))
        {
            basePath = Path.GetDirectoryName(basePath);
            possiblePath = Path.Combine(basePath ?? "./", filename);
        }

        if (!File.Exists(possiblePath))
        {
            return;
        }

        var fileContent = File.ReadAllText(possiblePath);

        var properties = platformMetadata.GetType()
            .GetProperties()
            .Where(x => x.IsDefined(typeof(EnvironmentVariableAttribute)));

        foreach (var property in properties)
        {
            var environmentVariableName = property.GetCustomAttribute<EnvironmentVariableAttribute>()?.VariableName;

            var propertyValue = GetFileVariable(fileContent, environmentVariableName);

            if (!string.IsNullOrWhiteSpace(propertyValue))
            {
                property.SetValue(platformMetadata, propertyValue);
            }
        }
    }
}
