﻿using System.Text;
using Microsoft.AspNetCore.Builder;
using Prometheus.Client.AspNetCore;

namespace Inozpavel.Microservices.Platform.Extensions.ApplicationBuilder;

internal static class PrometheusApplicationBuilderExtension
{
    public static IApplicationBuilder UsePlatformPrometheus(this IApplicationBuilder app)
    {
        app.UsePrometheusServer(options =>
        {
            options.MapPath = "/metrics";
            options.UseDefaultCollectors = true;
            options.ResponseEncoding = Encoding.UTF8;
        });

        return app;
    }
}
