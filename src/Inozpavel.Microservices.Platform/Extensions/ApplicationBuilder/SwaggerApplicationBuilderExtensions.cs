﻿using Inozpavel.Microservices.Platform.Common;
using Inozpavel.Microservices.Platform.Common.Enums;
using Inozpavel.Microservices.Platform.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.OpenApi.Models;

namespace Inozpavel.Microservices.Platform.Extensions.ApplicationBuilder;

internal static class ApplicationBuilderExtensions
{
    public static IApplicationBuilder UsePlatformSwagger(
        this IApplicationBuilder app,
        PlatformEnvironment platformEnvironment,
        PlatformMetadata platformMetadata)
    {
        if (platformEnvironment.SwaggerMode != SwaggerMode.Enabled)
        {
            return app;
        }

        app.RedirectDocsEndpoint();

        app.Use((context, next) =>
        {
            context.Response.Headers.TryAdd("Access-Control-Allow-Origin", "*");
            return next();
        });

        app.UseSwagger(options =>
        {
            options.RouteTemplate = "swagger/{documentName}.json";
            options.PreSerializeFilters.Add((swagger, request) =>
            {
                swagger.Servers = new List<OpenApiServer>
                {
                    new () { Url = $"{request.Scheme}://{request.Host.Value}" },
                };
            });
        });

        app.UseSwaggerUI(options =>
        {
            options.RoutePrefix = "swagger";
            options.DocumentTitle = platformMetadata.ApplicationName;
            options.SwaggerEndpoint("swagger.json", "Swagger");
            options.DisplayRequestDuration();

            if (platformEnvironment.SwaggerEnableTryItOutByDefault)
            {
                options.EnableTryItOutByDefault();
            }
        });

        return app;
    }

    /// <summary>
    /// Перенаправление конечной точки "/docs" в "/swagger"
    /// </summary>
    private static void RedirectDocsEndpoint(this IApplicationBuilder app)
    {
        app.Map("/docs",
            builder => builder.Use((HttpContext context, RequestDelegate _) =>
            {
                context.Response.Redirect("/swagger", true);
                return context.Response.CompleteAsync();
            }));
    }

    /// <summary>
    /// Добавляет список быстрых ссылок на debug порт, список будет доступен на конечной точке "/"
    /// </summary>
    public static IApplicationBuilder UsePlatformDashboard(
        this IApplicationBuilder app,
        PlatformEnvironment platformEnvironment)
    {
        return app.MapWhen(
            context =>
            {
                var debugPort = platformEnvironment.DebugPort;

                return debugPort.HasValue && context.Request.Host.Port == debugPort && context.Request.Path == "/";
            },
            builder => builder.UseMiddleware<PlatformDashboardMiddleware>());
    }

    /// <summary>
    /// Добавляет конечную точку /version" для просмотра информации о приложении
    /// </summary>
    public static IApplicationBuilder UsePlatformVersion(
        this IApplicationBuilder app,
        PlatformMetadata platformMetadata)
    {
        return app.Map("/version",
            builder => builder.UseMiddleware<PlatformVersionMiddleware>(platformMetadata));
    }

    /// <summary>
    /// Добавляет конечную точку /dependencies" для просмотра информации о зависимостях
    /// </summary>
    public static IApplicationBuilder UsePlatformDependencies(this IApplicationBuilder app)
    {
        return app.Map("/dependencies",
            builder => builder.UseMiddleware<PlatformDependenciesMiddleware>());
    }
}
