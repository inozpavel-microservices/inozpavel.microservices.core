﻿using Microsoft.Extensions.Configuration;

namespace Inozpavel.Microservices.Platform.Extensions;

internal static class ConfigurationBuilderExtension
{
    public static void DisableFilesSourcesReloadOnChange(this IConfigurationBuilder builder)
    {
        var sources = builder.Sources.OfType<FileConfigurationSource>().ToArray();
        foreach (var configurationSource in sources)
        {
            configurationSource.ReloadOnChange = false;
        }
    }
}
