﻿using Inozpavel.Microservices.Platform.Common;
using Inozpavel.Microservices.Platform.Configurators;
using Inozpavel.Microservices.Platform.Extensions.PlatformHostConfigurator.Defaults;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Inozpavel.Microservices.Platform;

public static class HostBuilderExtension
{
    // ReSharper disable once UnusedMethodReturnValue.Global
    // ReSharper disable once MemberCanBePrivate.Global
    public static IHostBuilder UsePlatform<TStartup>(
        this IHostBuilder hostBuilder,
        Action<PlatformEnvironment>? configureEnvironment = null) where TStartup : class
    {
        var applicationAssembly = typeof(TStartup).Assembly;
        var libraryAssembly = typeof(IPlatformHostConfigurator).Assembly;

        var metadata = new PlatformMetadata(applicationAssembly, libraryAssembly);

        var environment = new PlatformEnvironment();
        environment.InitializeFromEnvironmentVariables();
        configureEnvironment?.Invoke(environment);

        IPlatformHostConfigurator configurator = new PlatformHostConfigurator(hostBuilder);
        configurator.ConfigureDefaults(metadata, environment);

        metadata.CompleteInitialization();

        return configurator
            .Configure()
            .ConfigureWebHostDefaults(options => options.UseStartup<TStartup>());
    }
}
