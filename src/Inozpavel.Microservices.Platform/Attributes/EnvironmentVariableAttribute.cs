﻿namespace Inozpavel.Microservices.Platform.Attributes;

[AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
internal class EnvironmentVariableAttribute : Attribute
{
    public string VariableName { get; private set; }

    public EnvironmentVariableAttribute(string variableName)
    {
        VariableName = variableName;
    }
}
