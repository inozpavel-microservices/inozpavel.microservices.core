﻿using System.Reflection;
using Inozpavel.Microservices.Platform.Attributes;

// ReSharper disable MemberCanBePrivate.Global

namespace Inozpavel.Microservices.Platform.Common;

/// <summary>
/// Информация о приложении, изменяется только самой библиотекой или переменными среды
/// </summary>
internal class PlatformMetadata
{
    public PlatformMetadata(Assembly applicationAssembly, Assembly libraryAssembly)
    {
        StartTime = DateTimeOffset.Now;

        ApplicationName = applicationAssembly.GetName().Name;
        ApplicationCulture = applicationAssembly.GetName().CultureInfo?.EnglishName;
        ApplicationVersion = applicationAssembly.GetName().Version?.ToString();
        PlatformVersion = libraryAssembly.GetName().Version?.ToString();
        PlatformCulture = libraryAssembly.GetName().CultureInfo?.EnglishName;
        RuntimeVersion = System.Runtime.InteropServices.RuntimeInformation.FrameworkDescription;
    }

    public DateTimeOffset InitializeCompleted { get; private set; }

    [EnvironmentVariable("APPLICATION_NAME")]
    public string? ApplicationName { get; set; }

    [EnvironmentVariable("APPLICATION_DESCRIPTION")]
    public string? ApplicationDescription { get; set; }

    [EnvironmentVariable("APPLICATION_CONTACT_EMAIL")]
    public string? ApplicationContactEmail { get; set; }

    [EnvironmentVariable("APPLICATION_CONTACT_NAME")]
    public string? ApplicationContactName { get; set; }

    [EnvironmentVariable("APPLICATION_CONTACT_URL")]
    public string? ApplicationContactUrl { get; set; }

    #region AssembliesInformation

    public string? ApplicationCulture { get; }

    public string? ApplicationVersion { get; }

    public string RuntimeVersion { get; }

    public string? PlatformVersion { get; }

    public string? PlatformCulture { get; }

    #endregion

    [EnvironmentVariable("APPLICATION_VCS_BRANCH")]
    public string VcsBranch { get; set; }

    [EnvironmentVariable("APPLICATION_BUILD_TIME")]
    public DateTimeOffset? BuildTime { get; set; }

    public DateTimeOffset StartTime { get; }

    public void CompleteInitialization() => InitializeCompleted = DateTimeOffset.Now;
}
