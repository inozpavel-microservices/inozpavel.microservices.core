﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

namespace Inozpavel.Microservices.Platform.Common;

internal class PlatformStartupFilter : IStartupFilter
{
    private readonly Action<IApplicationBuilder, PlatformMetadata, PlatformEnvironment> _configure;
    private readonly PlatformMetadata _metadata;
    private readonly PlatformEnvironment _environment;

    public PlatformStartupFilter(
        Action<IApplicationBuilder, PlatformMetadata, PlatformEnvironment> configure,
        PlatformMetadata metadata,
        PlatformEnvironment environment)
    {
        _configure = configure;
        _metadata = metadata;
        _environment = environment;
    }

    public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next) =>
        app =>
        {
            _configure.Invoke(app, _metadata, _environment);
            next(app);
        };
}
