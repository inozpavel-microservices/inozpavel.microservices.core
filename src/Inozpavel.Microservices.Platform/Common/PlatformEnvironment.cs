﻿using System.Reflection;
using Inozpavel.Microservices.Platform.Attributes;
using Inozpavel.Microservices.Platform.Common.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using JsonConverter = Newtonsoft.Json.JsonConverter;

namespace Inozpavel.Microservices.Platform.Common;

/// <summary>
/// Информация о среде, в которой приложение работает.
/// Информация может быть изменена приложением, использующим эту библиотеку
/// </summary>
public class PlatformEnvironment
{
    /// <summary>
    /// Порт, на котором будет работь Kestrel
    /// </summary>
    [EnvironmentVariable("ASPNETCORE_HTTP_PORT")]
    public int HttpPort { get; set; } = 80;

    /// <summary>
    /// Порт, на котором будет работь Kestrel, по конечной точке "/" будут доступны быстрые ссылки
    /// </summary>
    [EnvironmentVariable("ASPNETCORE_DEBUG_PORT")]
    public int? DebugPort { get; set; } = null;

    /// <summary>
    /// Позволяет включить/отключить Swagger. Возможные значения: Enable, Disabled
    /// </summary>
    [EnvironmentVariable("ASPNETCORE_SWAGGER_MODE")]
    public SwaggerMode SwaggerMode { get; set; }

    /// <summary>
    /// Добавление security схемы для swagger. Можно выбрать несколько через |
    /// </summary>
    public SwaggerSecurity SwaggerSecurity { get; set; } = SwaggerSecurity.None;

    public bool SwaggerEnableTryItOutByDefault { get; set; } = true;

    public void InitializeFromEnvironmentVariables()
    {
        var properties = GetType()
            .GetProperties()
            .Where(x => x.IsDefined(typeof(EnvironmentVariableAttribute)))
            .ToArray();

        foreach (var propertyInfo in properties)
        {
            var variableName = propertyInfo.GetCustomAttribute<EnvironmentVariableAttribute>()?.VariableName;

            if (string.IsNullOrWhiteSpace(variableName))
                continue;

            var stringValue = $"\"{Environment.GetEnvironmentVariable(variableName)}\"";

            try
            {
                var variableValue = JsonConvert.DeserializeObject(
                    stringValue,
                    propertyInfo.PropertyType,
                    new JsonSerializerSettings
                    {
                        Converters = new List<JsonConverter>
                        {
                            new StringEnumConverter(),
                        },
                    });
                propertyInfo.SetValue(this, variableValue);
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}
