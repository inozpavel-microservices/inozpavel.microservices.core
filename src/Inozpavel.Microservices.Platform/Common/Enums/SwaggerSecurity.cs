﻿namespace Inozpavel.Microservices.Platform.Common.Enums;

[Flags]
public enum SwaggerSecurity
{
    None = 1 << 0,
    JwtBearer = 1 << 1,
}
