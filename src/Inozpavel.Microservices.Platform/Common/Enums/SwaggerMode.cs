﻿namespace Inozpavel.Microservices.Platform.Common.Enums;

/// <summary>
/// Настройка работы swagger
/// </summary>
public enum SwaggerMode
{
    /// <summary>
    /// Swagger добавляется, swagger-ui будет доступен на конечной точке /swagger
    /// </summary>
    Enabled = 0,

    /// <summary>
    /// Swagger не добавляется
    /// </summary>
    Disabled = 1,
}
