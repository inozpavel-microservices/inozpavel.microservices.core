﻿using System.Text;
using Inozpavel.Microservices.Platform.Middlewares.Abstractions;
using Microsoft.AspNetCore.Http;

namespace Inozpavel.Microservices.Platform.Middlewares;

/// <summary>
/// Middleware компонент для отображения быстрых ссылок
/// </summary>
internal sealed class PlatformDashboardMiddleware : PlatformMiddleware
{
    private readonly string[] _links =
    {
        "docs",
        "live",
        "metrics",
        "swagger",
        "version",
        "hangfire",
        "dependencies",
    };

    public PlatformDashboardMiddleware(RequestDelegate next) : base(next)
    {
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var builder = new StringBuilder();

        var orderedLinks = _links.OrderBy(x => x);

        foreach (var link in orderedLinks)
        {
            builder.AppendLine($"<a href =\"{link}\">{link}</a><br />");
        }

        context.Response.ContentType = "text/html";
        await context.Response.WriteAsync(builder.ToString());
    }
}
