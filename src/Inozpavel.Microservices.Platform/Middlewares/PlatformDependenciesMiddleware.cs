﻿using System.Reflection;
using Inozpavel.Microservices.Platform.Middlewares.Abstractions;
using Inozpavel.Microservices.Platform.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Inozpavel.Microservices.Platform.Middlewares;

/// <summary>
/// Middleware компонент для отображения количества зависимостей в проекте, их названий и версий
/// </summary>
internal sealed class PlatformDependenciesMiddleware : PlatformMiddleware
{
    public PlatformDependenciesMiddleware(RequestDelegate next) : base(next)
    {
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var assembly = Assembly.GetEntryAssembly();

        var dependencies = assembly?.GetReferencedAssemblies()
            .Select(x => new DependencyModel
            {
                Name = x.Name!,
                Version = x.Version?.ToString(),
            })
            .OrderBy(x => x.Name)
            .ToArray();

        var responseModel = new DependenciesModel
        {
            AssemblyName = assembly?.GetName().Name,
            Count = dependencies?.Length ?? 0,
            Dependencies = dependencies ?? Array.Empty<DependencyModel>(),
        };
        var result = JsonConvert.SerializeObject(responseModel, Formatting.Indented);

        context.Response.ContentType = "text/json";
        await context.Response.WriteAsync(result);
    }
}
