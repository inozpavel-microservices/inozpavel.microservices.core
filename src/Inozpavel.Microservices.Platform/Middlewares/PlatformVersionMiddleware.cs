﻿using Inozpavel.Microservices.Platform.Common;
using Inozpavel.Microservices.Platform.Middlewares.Abstractions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Inozpavel.Microservices.Platform.Middlewares;

/// <summary>
/// Middleware компонент для отображения информации о приложении
/// </summary>
internal sealed class PlatformVersionMiddleware : PlatformMiddleware
{
    private readonly PlatformMetadata _metadata;

    public PlatformVersionMiddleware(RequestDelegate next, PlatformMetadata metadata) : base(next)
    {
        _metadata = metadata;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var result = JsonConvert.SerializeObject(_metadata, Formatting.Indented);

        context.Response.ContentType = "text/json";

        await context.Response.WriteAsync(result);
    }
}
