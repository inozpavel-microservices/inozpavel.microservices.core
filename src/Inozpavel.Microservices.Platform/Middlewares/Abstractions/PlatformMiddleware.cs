﻿using Microsoft.AspNetCore.Http;

namespace Inozpavel.Microservices.Platform.Middlewares.Abstractions;

internal abstract class PlatformMiddleware
{
    // ReSharper disable once UnusedParameter.Local
    protected PlatformMiddleware(RequestDelegate next)
    {
        // Необходим для middleware
        // next не требуется вызывать, т.к. PlatformMiddleware это конечная точка
    }
}
