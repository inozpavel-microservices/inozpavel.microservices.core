﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Inozpavel.Microservices.Platform.Configurators;

internal class PlatformHostConfigurator : IPlatformHostConfigurator
{
    private readonly IHostBuilder _hostBuilder;

    public PlatformHostConfigurator(IHostBuilder hostBuilder)
    {
        _hostBuilder = hostBuilder;
    }

    public void ConfigureContext(Action<HostBuilderContext>? action) =>
        _hostBuilder.ConfigureServices((context, _) => action?.Invoke(context));

    public void ConfigureServices(Action<IServiceCollection>? configure) =>
        _hostBuilder.ConfigureServices((_, services) => configure?.Invoke(services));

    public void ConfigureKestrel(Action<KestrelServerOptions>? configure) =>
        _hostBuilder.ConfigureWebHost(builder => builder.ConfigureKestrel(options => configure?.Invoke(options)));

    public void ConfigureConfiguration(Action<IConfigurationBuilder>? configure) =>
        _hostBuilder.ConfigureAppConfiguration(builder => configure?.Invoke(builder));

    public IHostBuilder Configure() => _hostBuilder;
}
