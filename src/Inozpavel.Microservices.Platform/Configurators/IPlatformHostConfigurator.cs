﻿using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Inozpavel.Microservices.Platform.Configurators;

internal interface IPlatformHostConfigurator
{
    void ConfigureServices(Action<IServiceCollection> configure);

    void ConfigureKestrel(Action<KestrelServerOptions> configure);

    void ConfigureConfiguration(Action<IConfigurationBuilder> configure);

    void ConfigureContext(Action<HostBuilderContext> action);

    IHostBuilder Configure();
}
