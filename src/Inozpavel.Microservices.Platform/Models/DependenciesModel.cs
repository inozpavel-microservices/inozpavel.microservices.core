﻿namespace Inozpavel.Microservices.Platform.Models;

internal class DependenciesModel
{
    public string? AssemblyName { get; init; }

    public int Count { get; init; }

    public IEnumerable<DependencyModel> Dependencies { get; init; }
}
