﻿namespace Inozpavel.Microservices.Platform.Models;

internal class DependencyModel
{
    public string Name { get; init; }

    public string? Version { get; init; }
}
