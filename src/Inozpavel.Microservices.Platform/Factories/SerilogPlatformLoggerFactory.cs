﻿using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using ILogger = Serilog.ILogger;

namespace Inozpavel.Microservices.Platform.Factories;

internal class SerilogPlatformLoggerFactory : IPlatformLoggerFactory
{
    private readonly IConfiguration _configuration;

    public SerilogPlatformLoggerFactory(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    /// <summary>
    /// <a href="https://github.com/serilog/serilog-settings-configuration">Logging configuration</a>
    /// </summary>
    public ILogger CreateLogger()
    {
        var loggerConfiguration = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.Console(
                    outputTemplate:
                    "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}")
                .MinimumLevel.Information()
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
                .ReadFrom.Configuration(_configuration)
            ;

        var logger = loggerConfiguration.CreateLogger();
        return logger;
    }
}
