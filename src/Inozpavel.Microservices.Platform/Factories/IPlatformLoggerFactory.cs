﻿using Serilog;

namespace Inozpavel.Microservices.Platform.Factories;

public interface IPlatformLoggerFactory
{
    ILogger CreateLogger();
}
