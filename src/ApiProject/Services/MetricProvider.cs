﻿using Prometheus.Client;

namespace ApiProject.Services;

public class MetricProvider : IMetricProvider
{
    private readonly ICounter _weatherForecastRequestsCounter;

    public MetricProvider(IMetricFactory metricFactory)
    {
        _weatherForecastRequestsCounter = metricFactory.CreateCounter(
            "weather_forecast_requests_counter",
            "Total count of weather forecast requests");
    }

    public void IncWeatherForecastRequestsCounter()
    {
        _weatherForecastRequestsCounter.Inc();
    }
}
