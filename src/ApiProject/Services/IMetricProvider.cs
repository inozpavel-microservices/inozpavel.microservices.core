﻿namespace ApiProject.Services;

public interface IMetricProvider
{
    void IncWeatherForecastRequestsCounter();
}
