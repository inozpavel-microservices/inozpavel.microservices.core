﻿using System.IdentityModel.Tokens.Jwt;
using System.Text;
using ApiProject.Options;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace ApiProject.Controllers;

[ApiController]
[Route("api/token")]
public class TokenController : ControllerBase
{
    private readonly JwtOptions _jwtOptions;

    public TokenController(IOptions<JwtOptions> jwtOptions)
    {
        _jwtOptions = jwtOptions.Value ?? throw new ArgumentException(nameof(JwtOptions));
    }

    [HttpGet]
    public IActionResult GetToken()
    {
        var token = new JwtSecurityToken(
            _jwtOptions.Issuer,
            null,
            null,
            DateTime.Now,
            DateTime.Now.AddMinutes(_jwtOptions.ExpireIntervalInMinutes),
            new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.SecretKey)),
                SecurityAlgorithms.HmacSha256));

        var encodedJwt = new JwtSecurityTokenHandler().WriteToken(token);

        return Ok(encodedJwt);
    }

    [Authorize]
    [HttpGet("test")]
    public IActionResult TestToken() => Ok();
}
