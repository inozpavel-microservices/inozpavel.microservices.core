﻿using ApiProject.Models;
using ApiProject.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace ApiProject.Controllers;

/// <summary>
///     Основной контроллер для тестов библиотеки
/// </summary>
[ApiController]
[Route("weather-forecast")]
[SwaggerTag("Работа с погодой")]
public class WeatherForecastController : ControllerBase
{
    private static readonly string[] _summaries =
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching",
    };

    private readonly ILogger<WeatherForecastController> _logger;
    private readonly IMetricProvider _metricProvider;

    public WeatherForecastController(ILogger<WeatherForecastController> logger, IMetricProvider metricProvider)
    {
        _logger = logger;
        _metricProvider = metricProvider;
    }

    /// <summary>
    ///     Получение случайной информации о погоде
    /// </summary>
    [HttpGet("single")]
    [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(WeatherForecast))]
    public IActionResult LogWeather()
    {
        _metricProvider.IncWeatherForecastRequestsCounter();

        var weatherForecast = GetRandomWeatherForecast(1);
        _logger.LogInformation("Random weather: {@weatherForecast}", weatherForecast);

        return Ok(weatherForecast);
    }

    /// <summary>
    ///     Получение коллекции случайной информации о погоде
    /// </summary>
    [HttpGet("random-array")]
    [SwaggerResponse(StatusCodes.Status200OK, Type = typeof(IEnumerable<WeatherForecast>))]
    public IActionResult GetRandomWeather()
    {
        _metricProvider.IncWeatherForecastRequestsCounter();

        var result = Enumerable.Range(1, 5).Select(GetRandomWeatherForecast)
            .ToArray();

        return Ok(result);
    }

    private static WeatherForecast GetRandomWeatherForecast(int daysToAdd)
    {
        var random = new Random();

        return new WeatherForecast
        {
            Date = DateTime.Now.AddDays(daysToAdd),
            TemperatureC = random.Next(-20, 55),
            Summary = _summaries[random.Next(_summaries.Length)],
        };
    }
}
