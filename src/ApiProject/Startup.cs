using ApiProject.Extensions;
using ApiProject.Options;
using ApiProject.Services;
using FluentValidation.AspNetCore;
using Hellang.Middleware.ProblemDetails;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Prometheus.Client.HttpRequestDurations;

namespace ApiProject;

public class Startup
{
    private readonly IConfiguration _configuration;

    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services
            .AddProblemDetails(options => options.MapToStatusCode<Exception>(StatusCodes.Status500InternalServerError));

        services
            .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearerAuthentication(_configuration);

        services
            .AddSingleton<IMetricProvider, MetricProvider>()
            .Configure<JwtOptions>(_configuration.GetSection(nameof(JwtOptions)).Bind);

        services
            .AddControllers(options => options.Filters.Add(new ProducesAttribute("text/json")))
            .AddJsonOptions(options => options.JsonSerializerOptions.MaxDepth = 16)
            .AddFluentValidation(options => options.RegisterValidatorsFromAssemblyContaining<Startup>());
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseProblemDetails();

        app.UsePrometheusRequestDurations(options =>
        {
            options.IncludeMethod = true;
            options.IncludePath = true;
            options.IncludeStatusCode = true;
        });

        app.UseRouting();

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints => endpoints.MapControllers());
    }
}
