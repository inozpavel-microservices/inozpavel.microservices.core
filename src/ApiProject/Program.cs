using Inozpavel.Microservices.Platform;
using Inozpavel.Microservices.Platform.Common.Enums;

namespace ApiProject;

public static class Program
{
    public static void Main(string[] args)
    {
        var host = CreateHostBuilder(args).Build();

        host.Run();
    }

    private static IHostBuilder CreateHostBuilder(string[] args)
    {
        var host = Host
            .CreateDefaultBuilder(args)
            .UsePlatform<Startup>(options => options.SwaggerSecurity = SwaggerSecurity.JwtBearer);

        return host;
    }
}
