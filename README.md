# Inozpavel.Microservices.Platform

[Nuget файл для использования библиотеки](./Nuget.Config)

### Переменные среды

| Название                | Описание                                             | Возможные значения |
|-------------------------|------------------------------------------------------|--------------------|
| ASPNETCORE_HTTP_PORT    | Порт для http протокола                              | 5000               |
| ASPNETCORE_DEBUG_PORT   | Порт, на котором будет работать быстрое меню доступа | 5004               |
| ASPNETCORE_SWAGGER_MODE | Режим работы Swagger                                 | Enabled Disabled   |

### Переменные среды в .gitlab-cy.yml

| Название                  | Описание | Возможные значения | Пример |
|---------------------------| -------- | ------------------ | ------ |
| APPLICATION_NAME          | | | web-api-service |
| APPLICATION_DESCRIPTION   | | | Example of web api |
|                           | | | |
| APPLICATION_CONTACT_EMAIL | | | example@examplemail.ru |
| APPLICATION_CONTACT_NAME  | | | Ivanov Ivan |
| APPLICATION_CONTACT_URL   | | | https://gitlab.com | 
